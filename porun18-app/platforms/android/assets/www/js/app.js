$(document).foundation();

var OurApp = angular.module('our-app', ['ngRoute', 'mm.foundation']);


OurApp.config(['$routeProvider',
    function($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainController'
            })
            .when('/me/foods', {
                templateUrl: 'views/foods.html',
                controller: 'UserController'
            })
            .when('/food', {
                templateUrl: 'views/food/catalog.html',
                controller: 'FoodController'
            })
            .when('/food/:foodId', {
                templateUrl: 'views/food/product.html',
                controller: 'FoodController'
            })
            .when('/food/create/new', {
                templateUrl: 'views/food/new.html',
                controller: 'FoodController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
]);

OurApp.run(function($rootScope, clientPersistence, clientAPI) {
    var token = clientPersistence.retrieveToken();

    if (token != null) {
        clientAPI.getCurrentUser(token).then(function(response) {
            $rootScope.currentUser = response.data;
        });

    }
});

