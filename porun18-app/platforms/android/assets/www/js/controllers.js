OurApp.controller('FoodController', function ($scope, $modal, $rootScope, $location, $modalInstance, clientAPI, clientPersistence) {


    $scope.food = {};

    $scope.foodList = {};

    $scope.save = function(food) {
        var user = $rootScope.currentUser;
        clientAPI.food.create(user.token, food).then(function(response) {
          $modalInstance.close();
          $location.path('#/food/' + food.id);
        });
    };

    $scope.purchase = function(purchase) {
        var food = {
            offerId: offerId,
            portions: portions
        };


    };

    $scope.close = function() {
        $modalInstance.close();
    };

    //loadData();
});
OurApp.controller('IngredientController', function ($scope, $rootScope, $location) {


});
OurApp.controller('MainController', function ($scope, $rootScope, $modal, clientAPI, clientPersistence) {

    var token = clientPersistence.retrieveToken();

    $scope.foodList = [
        {
            name: "Chilaquiles en salsa verde",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-chilaquiles",
            pictureProfile: "face1.jpg",
            nameProfile: "Alejandra Gutierrez S.",
            rating: 2
        },
        {
            name: "Caldo de res y verduras",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-caldo",
            pictureProfile: "face2.jpg",
            nameProfile: "Francisco Murillo S.",
            rating: 5
        },
        {
            name: "Omelette con tocino",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-omelette",
            pictureProfile: "face4.jpg",
            nameProfile: "Gabriel Sandoval S.",
            rating: 1
        },
        {
            name: "Pechuga de pollo asada",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-pechuga",
            pictureProfile: "face5.jpg",
            nameProfile: "Paola Martinez R.",
            rating: 3
        }
    ];


    //function loadData() {
    //    var userId = '';
    //    clientAPI.getCurrentUser(token).then(function(user) {
    //        userId = user.data.id;
    //        return clientAPI.food.list(userId).then(function(response) {
    //            $scope.foodList = response.data;
    //        });
    //    });
    //
    //};



    $scope.open = function() {
        var modalInstance = $modal.open({
            templateUrl: 'sign-in.html',
            controller: 'SignInController'
        });


        modalInstance.result.then(function (selectedItem) {

        }, function () {

        });
    };

    $scope.openFood = function() {
        var modalInstance = $modal.open({
            templateUrl: 'new-food.html',
            controller: 'FoodController'
        });


        modalInstance.result.then(function (selectedItem) {

        }, function () {

        });
    };

    //loadData();

});
OurApp.controller('OfferController', function ($scope, $rootScope, $location) {


});
OurApp.controller('OrderController', function ($scope, $rootScope, $location) {

});
OurApp.controller('SignInController', function ($scope, $modal, $modalInstance, $rootScope, $location, clientAPI) {

    $scope.credentials = {};

    $scope.login = function(credentials) {
        // TODO Test Code
        //credentials.name     = 'Alex';
        //credentials.lastName = 'Acosta';
        //$rootScope.currentUser = credentials;


        clientAPI.signIn(credentials).then(function(user) {
            console.log(user);
            $rootScope.currentUser = user;
            console.log('Logging in...');
            $location.path('/');
        }, function(error) {
            $scope.error = true;
            $scope.errorMessage = 'Datos Incorrectos';
        });

        $modalInstance.close();
        // TODO Test Code
        $location.path('/me/foods');
    };

    $scope.close = function() {
        $modalInstance.close();
    };

    // Open SignUp Modal
    $scope.openSignUp = function() {

        $modalInstance.close();

        var modalInstance = $modal.open({
            templateUrl: 'sign-up.html',
            controller: 'SignUpController'
        });

        modalInstance.result.then(function (selectedItem) {

        }, function () {

        });
    };

});
OurApp.controller('SignOutController', function ($scope, $rootScope, $location, clientPersistence) {


    $scope.signOut = function() {

        clientPersistence.deleteToken();
        $rootScope.currentUser = null;
        $location.path('#/');
    };

});
OurApp.controller('SignUpController', function ($scope, $modalInstance, $rootScope, $location, clientAPI) {

    $scope.credentials = {};

    $scope.login = function(credentials) {

        clientAPI.signUp(credentials).then(function(user) {
            console.log(user);
            $rootScope.currentUser = user;
            console.log('Logging in...');
            $location.path('/me/foods');
        }, function(error) {
            $scope.error = true;
            $scope.errorMessage = 'Datos Incorrectos';
        });

        $modalInstance.close();
        // TODO Test Code
        $location.path('/me/foods');
    };

    $scope.close = function() {
        //    //$modalInstance.dismiss('cancel');
        $modalInstance.close();
    };

});
OurApp.controller('UserController', function ($scope, $rootScope, $location, clientAPI, clientPersistence) {

    var token = clientPersistence.retrieveToken();

    $scope.foodList = {};

    $scope.rate = function(productId, rate) {

        clientAPI.food.rate(productId, rate).then(function(response) {

        });
    };

    function loadData() {
        var userId = '';
        clientAPI.getCurrentUser(token).then(function(user) {
            userId = user.data.id;
            return clientAPI.food.list(userId).then(function(response) {
                $scope.foodList = response.data;
            });
        });
    };

    loadData();
});