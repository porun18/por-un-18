OurApp.controller('SignOutController', function ($scope, $rootScope, $location, clientPersistence) {


    $scope.signOut = function() {

        clientPersistence.deleteToken();
        $rootScope.currentUser = null;
        $location.path('#/');
    };

});