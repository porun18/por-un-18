OurApp.controller('SignUpController', function ($scope, $modalInstance, $rootScope, $location, clientAPI) {

    $scope.credentials = {};

    $scope.login = function(credentials) {

        clientAPI.signUp(credentials).then(function(user) {
            console.log(user);
            $rootScope.currentUser = user;
            console.log('Logging in...');
            $location.path('/me/foods');
        }, function(error) {
            $scope.error = true;
            $scope.errorMessage = 'Datos Incorrectos';
        });

        $modalInstance.close();
        // TODO Test Code
        $location.path('/me/foods');
    };

    $scope.close = function() {
        //    //$modalInstance.dismiss('cancel');
        $modalInstance.close();
    };

});