OurApp.controller('UserController', function ($scope, $rootScope, $location, clientAPI, clientPersistence) {

    var token = clientPersistence.retrieveToken();

    $scope.foodList = {};

    $scope.rate = function(productId, rate) {

        clientAPI.food.rate(productId, rate).then(function(response) {

        });
    };

    function loadData() {
        var userId = '';
        clientAPI.getCurrentUser(token).then(function(user) {
            userId = user.data.id;
            return clientAPI.food.list(userId).then(function(response) {
                $scope.foodList = response.data;
            });
        });
    };

    loadData();
});