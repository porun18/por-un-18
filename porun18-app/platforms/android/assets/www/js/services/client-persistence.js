OurApp.factory('clientPersistence', function() {

    return {
        storeToken: function(token) {
            localStorage.setItem("token", token);
        },

        deleteToken: function() {
            localStorage.removeItem("token");
        },

        retrieveToken: function() {
            return localStorage.getItem("token");
        }

    }

});