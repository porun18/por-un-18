OurApp.factory('clientsService', function($rootScope, $http, $q) {

    // Local Server Development
    //var serverURL = 'http://localhost:8080';

    var serverURL = 'http://192.168.0.26:8080';

    // Service Object
    var clientsService = {

        post: function (url, data) {
            var headers = {
                'Content-Type': 'application/json'
            }

            return $http.post(serverURL + '/' + url, data, { headers: headers} );
        },

        get: function (url) {
            return $http.get(serverURL + '/' + url);
        },

        put: function (url, data) {
            return $http.put(serverURL + '/' + url, data);
        }
    };

    return clientsService;
});