module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['lib/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'compressed',
          sourceMap: true,
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
    },

    // Concatenating the controllers files into one
    concat: {
      options: {
        //separator: ';',
      },
      controllers: {
        src: ['js/controllers/*.js', 'js/controllers/**/*.js'],
        dest: 'js/controllers.js',
      },
      services: {
        src: ['js/services/*.js', 'js/services/**/*.js'],
        dest: 'js/services.js',
      }
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass']
      },
        controllers: {
          files: 'js/controllers/**/*.js',
          tasks: ['concat:controllers'],
          options: {
            livereload: 35740
          }
        },
        services: {
          files: 'js/services/**/*.js',
          tasks: ['concat:services'],
          options: {
            livereload: 35740
          }
        }
    },

    connect: {
        server: {
            options: {
                port: 9999,
                base: '.'
            }
        }
    }
    
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('build', ['sass', 'concat']);
  grunt.registerTask('serve', ['build','connect','watch']);
  grunt.registerTask('default', ['build','watch']);
}
