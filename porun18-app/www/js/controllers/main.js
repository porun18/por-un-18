OurApp.controller('MainController', function ($scope, $rootScope, $modal, clientAPI, clientPersistence) {

    var token = clientPersistence.retrieveToken();

    $scope.foodList = [
        {
            name: "Chilaquiles en salsa verde",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-chilaquiles",
            pictureProfile: "face1.jpg",
            nameProfile: "Alejandra Gutierrez S.",
            rating: 2
        },
        {
            name: "Caldo de res y verduras",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-caldo",
            pictureProfile: "face2.jpg",
            nameProfile: "Francisco Murillo S.",
            rating: 5
        },
        {
            name: "Omelette con tocino",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-omelette",
            pictureProfile: "face4.jpg",
            nameProfile: "Gabriel Sandoval S.",
            rating: 1
        },
        {
            name: "Pechuga de pollo asada",
            ingredients: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            picture: "card-pechuga",
            pictureProfile: "face5.jpg",
            nameProfile: "Paola Martinez R.",
            rating: 3
        }
    ];


    //function loadData() {
    //    var userId = '';
    //    clientAPI.getCurrentUser(token).then(function(user) {
    //        userId = user.data.id;
    //        return clientAPI.food.list(userId).then(function(response) {
    //            $scope.foodList = response.data;
    //        });
    //    });
    //
    //};



    $scope.open = function() {
        var modalInstance = $modal.open({
            templateUrl: 'sign-in.html',
            controller: 'SignInController'
        });


        modalInstance.result.then(function (selectedItem) {

        }, function () {

        });
    };

    $scope.openFood = function() {
        var modalInstance = $modal.open({
            templateUrl: 'new-food.html',
            controller: 'FoodController'
        });


        modalInstance.result.then(function (selectedItem) {

        }, function () {

        });
    };

    //loadData();

});