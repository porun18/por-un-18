OurApp.controller('SignInController', function ($scope, $modal, $modalInstance, $rootScope, $location, clientAPI) {

    $scope.credentials = {};

    $scope.login = function(credentials) {
        // TODO Test Code
        //credentials.name     = 'Alex';
        //credentials.lastName = 'Acosta';
        //$rootScope.currentUser = credentials;


        clientAPI.signIn(credentials).then(function(user) {
            console.log(user);
            $rootScope.currentUser = user;
            console.log('Logging in...');
            $location.path('/');
        }, function(error) {
            $scope.error = true;
            $scope.errorMessage = 'Datos Incorrectos';
        });

        $modalInstance.close();
        // TODO Test Code
        $location.path('/me/foods');
    };

    $scope.close = function() {
        $modalInstance.close();
    };

    // Open SignUp Modal
    $scope.openSignUp = function() {

        $modalInstance.close();

        var modalInstance = $modal.open({
            templateUrl: 'sign-up.html',
            controller: 'SignUpController'
        });

        modalInstance.result.then(function (selectedItem) {

        }, function () {

        });
    };

});