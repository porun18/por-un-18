OurApp.controller('SignUpController', function ($scope, $modalInstance, $rootScope, $location, clientAPI) {

    $scope.credentials = {};

    $scope.signUp = function(credentials) {
        clientAPI.signUp(credentials).then(function(user) {
            console.log(user);
            $rootScope.currentUser = user;
        }, function(error) {
            $scope.error = true;
            $scope.errorMessage = 'Datos Incorrectos';
        });

        $modalInstance.close();
        // TODO Test Code
        $location.path('/');
    };

    $scope.close = function() {
        //    //$modalInstance.dismiss('cancel');
        $modalInstance.close();
    };

});