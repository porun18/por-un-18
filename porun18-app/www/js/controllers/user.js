OurApp.controller('UserController', function ($scope, $rootScope, $location, clientAPI, clientPersistence) {

    var token = clientPersistence.retrieveToken();

    $scope.foodList = {};

    $scope.rate = function(productId, rate) {

        clientAPI.food.rate(productId, rate).then(function(response) {

        });
    };

    $scope.offer = function(foodId) {
        var user = $rootScope.currentUser;
        var data = {
            "foodId": foodId,
            "portions":"5",
            "price": Math.floor((Math.random() * 60) + 1),
            "start":"2015-09-10T09:45:00.000+02:00",
            "end":"2015-09-15T09:45:00.000+02:00"
        };
        clientAPI.food.offer(user.token, data).then(function(response) {
            console.log(response);
            $location.path('#/');
        });
    };

    function loadData() {
        var userId = '';
        clientAPI.getCurrentUser(token).then(function(user) {
            userId = user.data.id;
            return clientAPI.food.list(userId).then(function(response) {
                $scope.foodList = response.data;
            });
        });
    };

    loadData();
});