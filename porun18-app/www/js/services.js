OurApp.factory('clientAPI', function($rootScope, clientsService, clientPersistence) {

    var user = $rootScope.currentUser;

    return {

        getFood: function() {
            return clientsService.get('/food');
        },

        signIn: function(credentials) {

            var data;
            return clientsService.post('users/login', credentials).then(function(response) {
                data = response.data;
                clientPersistence.storeToken(response.data.token);
                return clientPersistence.retrieveToken();
            }).then(function() {
                return data;
                //return clientsService.get('me/foods');
            });
        },

        signOut: function() {
            clientPersistence.deleteToken();

        },

        signUp: function(credentials) {
            return clientsService.post('/users/signup', credentials).then(function(response) {
                this.signIn(credentials);
            });
        },

        getCurrentUser: function(token) {
            return clientsService.get('users/me/' + token);
        },

        food: {
            create: function(token, food) {
                return clientsService.post('foods/add/' + token, food);
            },
            list: function(userId) {
                return clientsService.get('foods/get/' + userId);
            },
            get: function(foodId) {
                return clientsService.get('food/' + foodId);
            },
            purchase: function(foodId) {
                return clientsService.post('food/purchase', foodId);
            },
            rate: function(foodId, rate) {
                return clientsService.put('food/' + foodId, rate);
            }
        }
    }
});

OurApp.factory('clientPersistence', function() {

    return {
        storeToken: function(token) {
            localStorage.setItem("token", token);
        },

        deleteToken: function() {
            localStorage.removeItem("token");
        },

        retrieveToken: function() {
            return localStorage.getItem("token");
        }

    }

});
OurApp.factory('clientsService', function($rootScope, $http, $q) {

    // Local Server Development
    //var serverURL = 'http://localhost:8080';

    var serverURL = 'http://192.168.0.26:8080';

    // Service Object
    var clientsService = {

        post: function (url, data) {
            var headers = {
                'Content-Type': 'application/json'
            }

            return $http.post(serverURL + '/' + url, data, { headers: headers} );
        },

        get: function (url) {
            return $http.get(serverURL + '/' + url);
        },

        put: function (url, data) {
            return $http.put(serverURL + '/' + url, data);
        }
    };

    return clientsService;
});