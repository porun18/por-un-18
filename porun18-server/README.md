# Por un 18

## Server side

**Run:** 

	$ cd /porun18
	$ mvn spring-boot:run

**Open:**

	http://localhost:8090

## Client side

**Configure (only first time):** 

	$ cd /porun18/src/main/resources/static
	$ npm install

**Run:**

	$ cd /porun18/src/main/resources/static
	$ grunt

**Open:**

	http://localhost:9007