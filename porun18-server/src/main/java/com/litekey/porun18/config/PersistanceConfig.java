package com.litekey.porun18.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ch.qos.logback.core.net.server.Client;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.Order;
import com.litekey.porun18.model.Rating;
import com.litekey.porun18.model.User;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.litekey.porun18.persistence.impl" })
@PropertySource({ "classpath:persistence.properties" })
public class PersistanceConfig {

	@Value("${dataSource.driverClassName}")
	private String dataSourceDriverClassName;
	@Value("${dataSource.url}")
	private String dataSourceUrl;
	@Value("${dataSource.username}")
	private String dataSourceUsername;
	@Value("${dataSource.password}")
	private String dataSourcePassword;

	@Value("${hibernate.dialect}")
	private String hibernateDialect;
	@Value("${hibernate.show_sql}")
	private boolean hibernateShowSql;
	@Value("${hibernate.format_sql}")
	private boolean hibernateFormatSql;
	@Value("${hibernate.hbm2ddl.auto}")
	private String hibernateHbm2ddlAuto;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(dataSourceDriverClassName);
		ds.setUrl(dataSourceUrl);
		ds.setUsername(dataSourceUsername);
		ds.setPassword(dataSourcePassword);
		return ds;
	}

	private Properties hibernateProperties() {
		return new Properties() {

			private static final long serialVersionUID = 8021905948224002681L;

			{
				put("hibernate.dialect", hibernateDialect);
				put("hibernate.show_sql", hibernateShowSql);
				put("hibernate.format_sql", hibernateFormatSql);
				put("hibernate.hbm2ddl.auto", hibernateHbm2ddlAuto);
			}
		};
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sf = new LocalSessionFactoryBean();
		sf.setDataSource(dataSource());
		sf.setAnnotatedClasses(User.class, Offer.class,Client.class,
				Food.class,Ingredient.class,Order.class,Rating.class);
		sf.setHibernateProperties(hibernateProperties());
		return sf;
	}

	@Bean
	@Autowired
	public PlatformTransactionManager transactionManager(
			SessionFactory sessionFactory) {
		HibernateTransactionManager tm = new HibernateTransactionManager();
		tm.setSessionFactory(sessionFactory);
		return tm;
	}

}
