package com.litekey.porun18.config;

import javax.servlet.Filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.litekey.porun18.web.filters.CorsFilter;



@Configuration
@ComponentScan({ "com.litekey.porun18.services.impl" })
public class ServiceConfig {

	@Bean
	public Filter corsFilter(){
		return new CorsFilter();
	}

}
