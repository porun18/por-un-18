package com.litekey.porun18.exception;

public class UserExistException extends Exception{
	
	private static final long serialVersionUID = 123L;

	public UserExistException(){
		super();
	}
	
	public UserExistException(String message){
		super(message);
	}

}
