package com.litekey.porun18.exception;

public class UserNotValidException extends Exception{
	
	private static final long serialVersionUID = 1234L;

	public UserNotValidException(){
		super();
	}
	
	public UserNotValidException(String message){
		super(message);
	}

}
