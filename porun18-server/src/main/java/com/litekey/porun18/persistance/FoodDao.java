package com.litekey.porun18.persistance;

import java.util.List;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.model.User;

public interface FoodDao extends GenericDao<Food, Integer> {
	
	Food findByName(String name);
	List<Food> findByIngredients(List<Ingredient> ingredients);
	List<Food> findByUser(User user);
	List<Food> findByRating(Integer rating);
	List<Food> findByRatingUser(Integer rating, User user);
	List<Food> findByExtras(List<String> extras);
	List<Food> findByIngredientsExtras(List<Ingredient> ingredients,List<String> extras);

}
