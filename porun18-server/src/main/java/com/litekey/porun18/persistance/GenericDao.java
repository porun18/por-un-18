package com.litekey.porun18.persistance;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T extends Serializable, K extends Serializable> {
	
	void insert(T entity);

	void update(T entity);

	void delete(T entity);

	List<T> findAll();

	T find(K id);

}
