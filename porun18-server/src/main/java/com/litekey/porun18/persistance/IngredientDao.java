package com.litekey.porun18.persistance;

import java.util.List;

import com.litekey.porun18.model.Ingredient;


public interface IngredientDao extends GenericDao<Ingredient, Integer> {
	
	List<Ingredient> findByName(String name);

}
