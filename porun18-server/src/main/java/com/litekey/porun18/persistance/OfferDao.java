package com.litekey.porun18.persistance;

import java.util.Date;
import java.util.List;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.User;

public interface OfferDao extends GenericDao<Offer, Integer> {
	
	List<Offer> findByUser(User username);
	List<Offer> findByFood(Food food);
	List<Offer> findByPrice(float price);
	List<Offer> findByPortions(Integer portions);
	List<Offer> findByOffer(Boolean offer);
	List<Offer> findByDate(Date date);
	List<Offer> findByNew(Food food);
	List<Offer> findByCheap(Food food);
	List<Offer> findByBestRating(Food food);
	List<Offer> findByOffers(Food food);

}
