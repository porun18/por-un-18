package com.litekey.porun18.persistance;

import java.util.List;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Order;
import com.litekey.porun18.model.User;

public interface OrderDao extends GenericDao<Order, Integer> {
	
	List<Order> findByFood(Food food);
	List<Order> findByFoodSeller(Food food,User seller);
	List<Order> findBySeller(User seller);
	List<Order> findByFoodBuyer(Food food,User buyer);
	List<Order> findByBuyer(User buyer);

}
