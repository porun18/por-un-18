package com.litekey.porun18.persistance;

import java.util.List;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Rating;
import com.litekey.porun18.model.User;

public interface RatingDao extends GenericDao<Rating, Integer> {
	
	List<Rating> findBySeller(User seller);
	List<Rating> findByFood(Food food);
	List<Rating> findByBuyer(User buyer);
	Rating findByBuyerSeller(User buyer, User seller);
	Rating findByBuyerFood(User buyer, Food food);
}