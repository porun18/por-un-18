package com.litekey.porun18.persistance;

import com.litekey.porun18.model.User;

public interface UserDao extends GenericDao<User, Integer> {
	
	User find(String email);
	User find(String email, String password);
	User findByToken(String token);

}
