package com.litekey.porun18.persistance.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.FoodDao;

@Repository
public class FoodDaoHibernateImpl extends GenericDaoHibernateImpl<Food, Integer>
		implements FoodDao {

	public FoodDaoHibernateImpl() {
		super(Food.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Food findByName(String name) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.ilike("name", name))
				.list();
			Food food = null;
			if (foods.size() > 0) {
				food = foods.get(0);
			}
			return food;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Food> findByIngredients(List<Ingredient> ingredients) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.in("ingredients", ingredients))
				.list();
			return foods;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Food> findByUser(User user) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.eq("user", user))
				.list();
			return foods;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Food> findByRating(Integer rating) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.ge("rating", rating))
				.list();
			return foods;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Food> findByRatingUser(Integer rating, User user) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.ge("rating", rating))
				.add(Restrictions.eq("user", user))
				.list();
			return foods;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Food> findByExtras(List<String> extras) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.in("xtras", extras))
				.list();
			return foods;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Food> findByIngredientsExtras(List<Ingredient> ingredients,
			List<String> extras) {
		List<Food> foods = (List<Food>) createCriteria()
				.add(Restrictions.in("xtras", extras))
				.add(Restrictions.in("ingredients", ingredients))
				.list();
			return foods;
	}

}
