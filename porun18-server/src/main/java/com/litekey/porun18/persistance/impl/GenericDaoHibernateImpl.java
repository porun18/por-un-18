package com.litekey.porun18.persistance.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;

import com.litekey.porun18.persistance.GenericDao;

public class GenericDaoHibernateImpl<T extends Serializable, K extends Serializable>
		implements GenericDao<T, K> {

	private Class<? extends T> clazz;

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public GenericDaoHibernateImpl(Class<? extends T> clazz) {
		this.clazz = clazz;
	}

	public void insert(T entity) {
		getCurrentSession().save(entity);
	}

	public void update(T entity) {
		getCurrentSession().update(entity);
	}

	public void delete(T entity) {
		getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return (List<T>) getCurrentSession().createCriteria(clazz).list();
	}

	@SuppressWarnings("unchecked")
	public T find(K id) {
		return (T) getCurrentSession().get(clazz, id);
	}

	protected Criteria createCriteria() {
		return getCurrentSession().createCriteria(clazz);
	}

	protected Query createQuery(String hql) {
		return getCurrentSession().createQuery(hql);
	}

	protected SQLQuery createSqlQuery(String sql) {

		return getCurrentSession().createSQLQuery(sql);
	}

	protected Query getNamedQuery(String namedQuery) {
		return getCurrentSession().getNamedQuery(namedQuery);
	}

	protected void doWork(Work work) {
		getCurrentSession().doWork(work);
	}

}
