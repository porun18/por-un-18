package com.litekey.porun18.persistance.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.persistance.IngredientDao;

@Repository
public class IngredientDaoHibernateImpl extends GenericDaoHibernateImpl<Ingredient, Integer>
		implements IngredientDao {

	public IngredientDaoHibernateImpl() {
		super(Ingredient.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Ingredient> findByName(String name) {
		List<Ingredient> ingredients = (List<Ingredient>) createCriteria()
				.add(Restrictions.ilike("name", name))
				.list();
			
			return ingredients;
	}

}
