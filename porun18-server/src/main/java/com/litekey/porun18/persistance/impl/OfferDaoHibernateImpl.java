package com.litekey.porun18.persistance.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.OfferDao;

@Repository
public class OfferDaoHibernateImpl extends GenericDaoHibernateImpl<Offer, Integer>
		implements OfferDao {

	public OfferDaoHibernateImpl() {
		super(Offer.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByUser(User user) {
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.eq("user", user))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByFood(Food food) {
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.eq("food", food))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByPrice(float price) {
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("price", price))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByPortions(Integer portions) {
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("portions", portions))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByOffer(Boolean offer) {
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.eq("offer", offer))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByDate(Date date) {
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("date", date))
				.addOrder(Order.asc("date"))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByNew(Food food) {
		Date now = new Date();
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("end", now))
				.add(Restrictions.ge("start", now))
				.add(Restrictions.gt("availablePortions", 0))
				.addOrder(Order.desc("date"))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByCheap(Food food) {
		Date now = new Date();
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("end", now))
				.add(Restrictions.ge("start", now))
				.add(Restrictions.gt("availablePortions", 0))
				.addOrder(Order.asc("price"))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByBestRating(Food food) {
		Date now = new Date();
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("end", now))
				.add(Restrictions.ge("start", now))
				.add(Restrictions.gt("availablePortions", 0))
				.createAlias("food", "f")
				.addOrder(Order.desc("f.rating"))
				.list();
			return offers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Offer> findByOffers(Food food) {
		Date now = new Date();
		List<Offer> offers = (List<Offer>) createCriteria()
				.add(Restrictions.le("offer", true))
				.add(Restrictions.le("end", now))
				.add(Restrictions.ge("start", now))
				.add(Restrictions.gt("availablePortions", 0))
				.addOrder(Order.asc("price"))
				.list();
			return offers;
	}

}
