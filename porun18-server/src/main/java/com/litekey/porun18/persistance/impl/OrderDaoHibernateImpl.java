package com.litekey.porun18.persistance.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Order;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.OrderDao;

@Repository
public class OrderDaoHibernateImpl extends GenericDaoHibernateImpl<Order, Integer>
		implements OrderDao {

	public OrderDaoHibernateImpl() {
		super(Order.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Order> findByFood(Food food) {
		List<Order> orders = (List<Order>) createCriteria()
				.add(Restrictions.eq("food", food))
				.list();
			return orders;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Order> findByFoodSeller(Food food, User seller) {
		List<Order> orders = (List<Order>) createCriteria()
				.add(Restrictions.eq("food", food))
				.add(Restrictions.eq("seller", seller))
				.list();
			return orders;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Order> findBySeller(User seller) {
		List<Order> orders = (List<Order>) createCriteria()
				.add(Restrictions.eq("seller", seller))
				.list();
			return orders;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Order> findByFoodBuyer(Food food, User buyer) {
		List<Order> orders = (List<Order>) createCriteria()
				.add(Restrictions.eq("food", food))
				.add(Restrictions.eq("buyer", buyer))
				.list();
			return orders;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Order> findByBuyer(User buyer) {
		List<Order> orders = (List<Order>) createCriteria()
				.add(Restrictions.eq("buyer", buyer))
				.list();
			return orders;
	}

}
