package com.litekey.porun18.persistance.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Rating;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.RatingDao;

@Repository
public class RatingDaoHibernateImpl extends GenericDaoHibernateImpl<Rating, Integer>
		implements RatingDao {

	public RatingDaoHibernateImpl() {
		super(Rating.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Rating> findBySeller(User seller) {
		List<Rating> ratings = (List<Rating>) createCriteria()
				.add(Restrictions.eq("seller", seller))
				.list();
			return ratings;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Rating> findByFood(Food food) {
		List<Rating> ratings = (List<Rating>) createCriteria()
				.add(Restrictions.eq("food", food))
				.list();
			return ratings;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Rating> findByBuyer(User buyer) {
		List<Rating> ratings = (List<Rating>) createCriteria()
				.add(Restrictions.eq("buyer", buyer))
				.list();
			return ratings;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Rating findByBuyerSeller(User buyer, User seller) {
		List<Rating> ratings = (List<Rating>) createCriteria()
				.add(Restrictions.eq("buyer", buyer))
				.add(Restrictions.eq("seller", seller))
				.list();
		if(ratings.isEmpty()){
			return null;
		}else{
			return ratings.get(0);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Rating findByBuyerFood(User buyer, Food food) {
		List<Rating> ratings = (List<Rating>) createCriteria()
				.add(Restrictions.eq("buyer", buyer))
				.add(Restrictions.eq("food", food))
				.list();
		
		if(ratings.isEmpty()){
			return null;
		}else{
			return ratings.get(0);
		}
	}

}
