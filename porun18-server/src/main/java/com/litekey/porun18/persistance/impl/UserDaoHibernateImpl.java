package com.litekey.porun18.persistance.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.UserDao;

@Repository
public class UserDaoHibernateImpl extends GenericDaoHibernateImpl<User, Integer>
		implements UserDao {

	public UserDaoHibernateImpl() {
		super(User.class);
	}

	public User find(String email) {
		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) createCriteria().add(Restrictions.eq("email", email)).list();
		
		User user = null;
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}
	
	public User find(String email, String password) {
		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) createCriteria()
			.add(Restrictions.eq("email", email))
			.add(Restrictions.eq("password", password))
			.list();
		
		User user = null;
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}
	
	public User findByToken(String token) {
		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) createCriteria().add(Restrictions.eq("token", token)).list();
		
		User user = null;
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}

}
