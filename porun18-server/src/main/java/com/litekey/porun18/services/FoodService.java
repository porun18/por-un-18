package com.litekey.porun18.services;

import java.util.List;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.model.User;

public interface FoodService {
	
	Food getFoodById(Integer id);
	Food findByName(String name);
	List<Food> findByIngredients(List<Ingredient> ingredients);
	List<Food> findByUser(User user);
	List<Food> findByRating(Integer rating);
	List<Food> findByRatingUser(Integer rating, User user);
	
	List<Food> findAllAvailable();

	void addFood(Food food);
	void deleteFood(Food food);
}
