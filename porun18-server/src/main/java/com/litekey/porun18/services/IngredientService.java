package com.litekey.porun18.services;

import com.litekey.porun18.model.Ingredient;

public interface IngredientService {
	
	Ingredient getIngredientById(Integer id);
	Ingredient getIngredientByName(String name);

}
