package com.litekey.porun18.services;

import java.util.Date;
import java.util.List;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.User;

public interface OfferService {
	
	void addOffer(Offer offer);
	void updateOffer(Offer offer);
	void deleteOffer(Offer offers);
	List<Offer> findNew(Food food);
	List<Offer> findcheap(Food food);
	List<Offer> findBest(Food food);
	List<Offer> findOffers(Food food);
	Offer getOfferById(Integer id);
	List<Offer> findByUser(User username);
	List<Offer> findByFood(Food food);
	List<Offer> findByPrice(float price);
	List<Offer> findByPortions(Integer portions);
	List<Offer> findByOffer(Boolean offer);
	List<Offer> findByDate(Date date);

}
