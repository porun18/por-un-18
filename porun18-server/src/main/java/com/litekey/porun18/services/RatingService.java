package com.litekey.porun18.services;

import com.litekey.porun18.model.Rating;

public interface RatingService {
	
	void rate(Rating rating);

}
