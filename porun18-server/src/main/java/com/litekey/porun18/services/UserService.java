package com.litekey.porun18.services;

import java.util.List;

import com.litekey.porun18.exception.UserExistException;
import com.litekey.porun18.exception.UserNotValidException;
import com.litekey.porun18.model.User;

public interface UserService {
	
	User getByToken(String token);
	boolean logout(String token);
	User validate(User user) throws UserNotValidException;
	void addUser(User user) throws UserExistException;
	
	User getById(Integer id);
	List<User> allUsers();

}
