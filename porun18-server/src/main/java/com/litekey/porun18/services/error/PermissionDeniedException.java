package com.litekey.porun18.services.error;

import com.litekey.porun18.model.User;

public class PermissionDeniedException extends Exception {

	private static final long serialVersionUID = -6692735017919839564L;

	private User user;

	public PermissionDeniedException(User user, String message) {
		super(message);
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}
