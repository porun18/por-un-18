package com.litekey.porun18.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.FoodDao;
import com.litekey.porun18.services.FoodService;

@Service
public class FoodServiceDefaultImpl implements FoodService {

	@Autowired
	private FoodDao foodDao;

	@Override
	@Transactional(readOnly = true)
	public Food getFoodById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Food findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Food> findByIngredients(List<Ingredient> ingredients) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Food> findByUser(User user) {
		List<Food> foods = foodDao.findByUser(user);
		List<Food> foodsFilter = new ArrayList<Food>();
		for(Food food :foods){
			if(!foodsFilter.contains(food)){
				foodsFilter.add(food);
			}
		}
		return foodsFilter;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Food> findByRating(Integer rating) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Food> findByRatingUser(Integer rating, User user) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional
	public void addFood(Food food){
		foodDao.insert(food);
		System.out.println();
	}
	
	@Override
	@Transactional
	public void deleteFood(Food food){
		foodDao.delete(food);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Food> findAllAvailable() {
		return foodDao.findAll();
	}

}
