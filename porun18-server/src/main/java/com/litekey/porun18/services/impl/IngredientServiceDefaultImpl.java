package com.litekey.porun18.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.persistance.IngredientDao;
import com.litekey.porun18.services.IngredientService;

@Service
public class IngredientServiceDefaultImpl implements IngredientService {

	@Autowired
	private IngredientDao ingredientDao;

	@Override
	@Transactional(readOnly = true)
	public Ingredient getIngredientById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Ingredient getIngredientByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
