package com.litekey.porun18.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.OfferDao;
import com.litekey.porun18.services.OfferService;

@Service
public class OfferServiceDefaultImpl implements OfferService {

	@Autowired
	private OfferDao offerDao;

	@Override
	@Transactional(readOnly = true)
	public Offer getOfferById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Offer> findByUser(User username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Offer> findByFood(Food food) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Offer> findByPrice(float price) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Offer> findByPortions(Integer portions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Offer> findByOffer(Boolean offer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Offer> findByDate(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public List<Offer> findNew(Food food) {
		return offerDao.findByNew(food);
	}

	@Override
	@Transactional
	public List<Offer> findcheap(Food food) {
		return offerDao.findByCheap(food);
	}

	@Override
	@Transactional
	public List<Offer> findBest(Food food) {
		return offerDao.findByBestRating(food);
	}

	@Override
	@Transactional
	public List<Offer> findOffers(Food food) {
		return offerDao.findByOffers(food);
	}

	@Override
	@Transactional
	public void addOffer(Offer offer) {
		offerDao.insert(offer);
	}

	@Override
	@Transactional
	public void updateOffer(Offer offer) {
		offerDao.update(offer);
	}

	@Override
	@Transactional
	public void deleteOffer(Offer offer) {
		offerDao.delete(offer);
	}

}
