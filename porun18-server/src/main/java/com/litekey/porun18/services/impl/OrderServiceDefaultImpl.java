package com.litekey.porun18.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.Order;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.OfferDao;
import com.litekey.porun18.persistance.OrderDao;
import com.litekey.porun18.services.OrderService;

@Service
public class OrderServiceDefaultImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private OfferDao offerDao;

	@Override
	@Transactional(readOnly = true)
	public Order getOrderById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Order> findByFood(Food food) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Order> findByFoodSeller(Food food, User seller) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Order> findBySeller(User seller) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Order> findByFoodBuyer(Food food, User buyer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Order> findByBuyer(User buyer) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional
	public void addOrder(Order order){
		orderDao.insert(order);
		Offer offer = offerDao.find(order.getId());
		Integer available = offer.getAvailablePortions() - order.getPortions();
		if(offer.getAvailablePortions() < order.getPortions()){
			order.setPortions(offer.getAvailablePortions());
			offer.setAvailablePortions(0);
			offerDao.update(offer);
			orderDao.update(order);
		}else{
			offer.setAvailablePortions(available);
			offerDao.update(offer);
		}
		
	}

}
