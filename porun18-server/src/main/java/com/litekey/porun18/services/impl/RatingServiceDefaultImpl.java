package com.litekey.porun18.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Rating;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.FoodDao;
import com.litekey.porun18.persistance.RatingDao;
import com.litekey.porun18.persistance.UserDao;
import com.litekey.porun18.services.RatingService;

@Service
public class RatingServiceDefaultImpl implements RatingService {

	@Autowired
	private RatingDao ratingDao;
	
	@Autowired
	private FoodDao foodDao;
	
	@Autowired
	private UserDao userDao;

	@Override
	@Transactional
	public void rate(Rating rating) {
		if(rating.getSeller() == null && rating.getFood() != null){
			rateFood(rating);
		}else if(rating.getSeller() != null && rating.getFood() == null){
			rateSeller(rating);
		}
	}
	
	private void rateFood(Rating rating){
		Rating exist = ratingDao.findByBuyerFood(rating.getBuyer(), rating.getFood()); 
		if(exist == null){
			ratingDao.insert(rating);
		}else{
			exist.setRating(rating.getRating());
			ratingDao.update(rating);
		}
		updateFoodRating(rating.getFood());
	}
	
	private void rateSeller(Rating rating){
		Rating exist = ratingDao.findByBuyerSeller(rating.getBuyer(), rating.getSeller()); 
		if(exist == null){
			ratingDao.insert(rating);
		}else{
			exist.setRating(rating.getRating());
			ratingDao.update(rating);
		}
		updateSellerRating(rating.getSeller());
	}
	

	private void updateFoodRating(Food food){
		food = foodDao.find(food.getId());
		
		List<Rating> ratings = ratingDao.findByFood(food);
		Integer sum = 0;
		for(Rating r : ratings){
			sum = sum + ((r.getRating()!=null)?r.getRating():0);
		}
		
		food.setRating((Integer) sum/ratings.size());
		
		
		foodDao.update(food);
	}
	
	private void updateSellerRating(User seller){
		seller = userDao.find(seller.getId());
		
		List<Rating> ratings = ratingDao.findBySeller(seller);
		Integer sum = 0;
		for(Rating r : ratings){
			sum = sum + ((r.getRating()!=null)?r.getRating():0);
		}
		
		seller.setRating((Integer) sum/ratings.size());
		
		userDao.update(seller);
	}

}
