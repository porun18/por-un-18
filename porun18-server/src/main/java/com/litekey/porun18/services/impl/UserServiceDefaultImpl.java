package com.litekey.porun18.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.litekey.porun18.exception.UserExistException;
import com.litekey.porun18.exception.UserNotValidException;
import com.litekey.porun18.model.User;
import com.litekey.porun18.persistance.UserDao;
import com.litekey.porun18.services.UserService;

@Service
public class UserServiceDefaultImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Transactional(readOnly = true)
	public User getUser(String email) {
		return userDao.find(email);
	}
	
	@Override
	@Transactional
	public User validate(User user) throws UserNotValidException {
		User u = userDao.find(user.getEmail(), user.getPassword());
		if(u == null){
			throw new UserNotValidException("Usuario invalido");
		}else{
			u.setToken(generateToken());
			userDao.update(u);
		}
		return u;
	}
	
	@Override
	@Transactional(readOnly = true)
	public User getByToken(String token){
		return userDao.findByToken(token);
		
	}
	
	@Override
	@Transactional(readOnly = true)
	public User getById(Integer id){
		return userDao.find(id);
		
	}
	
	@Override
	@Transactional(readOnly = false)
	public boolean logout(String token){
		User u = userDao.findByToken(token);
		if(u != null){
			u.setToken(null);
			userDao.update(u);
			return true;
		}
		return false;
	}
	
	@Transactional
	public void addUser(User user) throws UserExistException{
		if(userDao.find(user.getEmail()) == null){
			
			user.setToken(generateToken());
			//user.setClient(c);
			userDao.insert(user);
			
		}else{
			throw new UserExistException("User Exist");
		}
	}

	
	private String generateToken() {
		StringBuffer buffer = new StringBuffer();
		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for (int i = 0; i < 40; i++) {
			double index = Math.random() * characters.length();
			buffer.append(characters.charAt((int) index));
		}
		return buffer.toString();
	}

	@Transactional(readOnly = true)
	public List<User> allUsers(){
	 List<User> users = userDao.findAll();
	 return users;
 }
}
