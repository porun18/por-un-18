package com.litekey.porun18.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.litekey.porun18.exception.UserExistException;
import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.Order;
import com.litekey.porun18.model.Rating;
import com.litekey.porun18.model.User;
import com.litekey.porun18.services.FoodService;
import com.litekey.porun18.services.IngredientService;
import com.litekey.porun18.services.OfferService;
import com.litekey.porun18.services.OrderService;
import com.litekey.porun18.services.RatingService;
import com.litekey.porun18.services.UserService;

@RestController
@RequestMapping("/data")
public class DataController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private FoodService foodService;
	
	@Autowired
	private IngredientService ingredientService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OfferService offerService;
	
	@Autowired
	private RatingService ratingService;
	
	@RequestMapping(value="",method = RequestMethod.GET)
	public void creatingData() throws UserExistException{
		addUser("erik.mendoza@litekey.mx","laquesea","Erik","Mendoza");
		addUser("jhonathan.osuna@litekey.mx","laquesea","Jhonathan","Osuna");
		addUser("alex.acosta@liteket.mx","laquesea","Alex","Acosta");
		addUser("omar.patino@litekey.mx","laquesea","Omar","Patiño");
		addUser("walter.hurtado@litekey.mx","laquesea","Walter","Hurtado");
		
		addFood("chilaquiles","chile, tortilla");
		addFood("quesadilla de flor de calabaza con queso","queso, flor de calabaza, tortillas");
		addFood("Pozole de Cerdo","cerdo, grano, repollo");
		addFood("enchiladas de pollo","chile rojo, tortillas");
		addFood("Tacos de pezcado","tortilla, pezcado");
		
		addOffer(foodService.findAllAvailable().get(0),7,5,35.50f,true,new Date(),new Date(),new Date());
		addOffer(foodService.findAllAvailable().get(1),5,2,15.50f,true,new Date(),new Date(),new Date());
		addOffer(foodService.findAllAvailable().get(2),10,10,45f,true,new Date(),new Date(),new Date());
		addOffer(foodService.findAllAvailable().get(3),15,1,25.50f,true,new Date(),new Date(),new Date());
		addOffer(foodService.findAllAvailable().get(4),8,1,30f,true,new Date(),new Date(),new Date());
		
		addOrder(foodService.findAllAvailable().get(0),userService.allUsers().get(0),userService.allUsers().get(3),1);
		addOrder(foodService.findAllAvailable().get(1),userService.allUsers().get(1),userService.allUsers().get(2),3);
		addOrder(foodService.findAllAvailable().get(2),userService.allUsers().get(3),userService.allUsers().get(1),2);
		addOrder(foodService.findAllAvailable().get(3),userService.allUsers().get(4),userService.allUsers().get(0),4);
		addOrder(foodService.findAllAvailable().get(4),userService.allUsers().get(2),userService.allUsers().get(4),2);

		addRating(userService.allUsers().get(0),userService.allUsers().get(3),5);
		addRating(foodService.findAllAvailable().get(1),userService.allUsers().get(2),4);
		addRating(userService.allUsers().get(3),userService.allUsers().get(1),2);
		addRating(foodService.findAllAvailable().get(3),userService.allUsers().get(0),5);
		addRating(userService.allUsers().get(2),userService.allUsers().get(4),2);
	}
	
	
	public void addUser(String email, String password, String name, String middleName) throws UserExistException{
		User user = new User();
		user.setEmail(email);
		user.setPassword(password);
		user.setName(name);
		user.setMiddleName(middleName);
		userService.addUser(user);
	}
	
	public void addFood(String name, String ingredients){
		Food food = new Food();
		food.setName(name);
		List<Ingredient> ingredient = new ArrayList<Ingredient>();
		Ingredient ing = new Ingredient();
		ing.setName(ingredients);
		ingredient.add(ing);
		foodService.addFood(food);
	}

	public void addOffer(Food food, Integer portions, Integer availablePortions, float price, boolean offer, Date date, Date start, Date end){
		Offer offert = new Offer();
		offert.setFood(food);
		offert.setPortions(availablePortions);
		offert.setAvailablePortions(availablePortions);
		offert.setPrice(price);
		offert.setOffer(offer);
		offert.setDate(date);
		offert.setEnd(end);
		offert.setStart(start);
		offerService.addOffer(offert);
	}
	
	public void addOrder(Food food, User buyer, User seller, Integer portions){
		Order order = new Order();
		//order.setFood(food);
		order.setBuyer(buyer);
		//order.setSeller(seller);
		order.setPortions(portions);
		orderService.addOrder(order);
	}
	
	public void addRating(User seller, User buyer, Integer rating){
		Rating ring = new Rating();
		ring.setSeller(seller);
		ring.setBuyer(buyer);
		ring.setRating(rating);
		//ratingService.addRating(ring);
	}
	
	public void addRating(Food food, User buyer, Integer rating){
		Rating ring = new Rating();
		ring.setFood(food);
		ring.setBuyer(buyer);
		ring.setRating(rating);
		//ratingService.addRating(ring);
	}
}
