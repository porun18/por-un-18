package com.litekey.porun18.web.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.User;
import com.litekey.porun18.services.FoodService;
import com.litekey.porun18.services.UserService;
import com.litekey.porun18.web.forms.FoodForm;

@RestController
@RequestMapping("/foods")
public class FoodController {

	@Autowired
	private FoodService foodService;

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/add/{token}", method = RequestMethod.POST)
	public void addFood(@PathVariable String token, @Valid @RequestBody FoodForm foodForm){
		Food food = foodForm.extract();
		User u = userService.getByToken(token);
		food.setUser(u);
		foodService.addFood(food);
	}
	
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Food> getFoods(@PathVariable Integer id){
		User u = userService.getById(id);
		return foodService.findByUser(u);
	}
	
	@RequestMapping(value = "/{token}/{foodId}",method = RequestMethod.DELETE)
	public void deleteFood(@PathVariable String token, @PathVariable String foodId){

	}
	


}
