package com.litekey.porun18.web.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.litekey.porun18.exception.UserExistException;
import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.User;
import com.litekey.porun18.services.OfferService;
import com.litekey.porun18.services.UserService;
import com.litekey.porun18.web.forms.OfferForm;

@RestController
@RequestMapping("/offers")
public class OfferController {

	@Autowired
	private OfferService offerService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/add/{token}", method = RequestMethod.POST)
	public void add(@PathVariable String token, @RequestBody OfferForm offerForm) {
		User u = userService.getByToken(token);
		Offer offer = offerForm.extract();
		try{
			offerService.addOffer(offer);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void delete(@Valid @RequestBody OfferForm offerForm) {
		Offer offer = offerForm.extract();
		try{
			offerService.deleteOffer(offer);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public void update(@Valid @RequestBody OfferForm offerForm) throws UserExistException {
		Offer offer = offerForm.extract();
		try{
			offerService.updateOffer(offer);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public @ResponseBody List<Offer> newOffers() {
		try{
			return offerService.findNew(null);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/cheap", method = RequestMethod.GET)
	public @ResponseBody List<Offer> cheapOffers() {
		try{
			return offerService.findcheap(null);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/best", method = RequestMethod.GET)
	public @ResponseBody List<Offer> bestOffers() {
		try{
			return offerService.findBest(null);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/offers", method = RequestMethod.GET)
	public @ResponseBody List<Offer> offers() {
		try{
			return offerService.findOffers(null);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}

}
