package com.litekey.porun18.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.litekey.porun18.model.Order;
import com.litekey.porun18.model.User;
import com.litekey.porun18.services.OrderService;
import com.litekey.porun18.services.UserService;
import com.litekey.porun18.web.forms.OrderForm;

@RestController
@RequestMapping("/orders")
public class OrdersController {

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/add/{token}", method = RequestMethod.POST)

	public void add(@PathVariable String token, @RequestBody OrderForm orderForm) {
		User u = userService.getByToken(token);
		Order order = orderForm.extract();
		order.setBuyer(u);
		try{
			orderService.addOrder(order);
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw e;
		}
	}


}
