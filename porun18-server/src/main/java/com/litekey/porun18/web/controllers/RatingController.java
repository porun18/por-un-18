package com.litekey.porun18.web.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.litekey.porun18.model.Rating;
import com.litekey.porun18.services.RatingService;
import com.litekey.porun18.services.UserService;
import com.litekey.porun18.web.forms.RatingForm;

@RestController
@RequestMapping("/rating")
public class RatingController {

	@Autowired
	private RatingService ratingService;
	
	@Autowired
	private UserService userService;	

	@RequestMapping(value = "/{token}", method = RequestMethod.POST)
	public void rate(@PathVariable String token, @Valid @RequestBody RatingForm ratingForm){
		Rating r = ratingForm.extract();
		r.setBuyer(userService.getByToken(token));
		ratingService.rate(r);
	}

}
