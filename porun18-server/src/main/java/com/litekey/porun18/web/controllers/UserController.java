package com.litekey.porun18.web.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.litekey.porun18.exception.UserExistException;
import com.litekey.porun18.exception.UserNotValidException;
import com.litekey.porun18.model.User;
import com.litekey.porun18.services.UserService;
import com.litekey.porun18.web.forms.UserForm;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody User login(@Valid @RequestBody UserForm userForm) throws UserNotValidException {
		try{
			return userService.validate(userForm.extract());
		}catch(UserNotValidException e){
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/logout/{token}", method = RequestMethod.GET)
	public void logout(@PathVariable String token){
		userService.logout(token);
	}
	
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public @ResponseBody User  getById(@PathVariable Integer id){
		User u = userService.getById(id);
		System.out.println(u);
		return u;
	}
	
	@RequestMapping(value = "/me/{token}", method = RequestMethod.GET)
	public @ResponseBody User getCredentials(@PathVariable String token){
		User u = userService.getByToken(token);
		System.out.println(u);
		return u;
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public @ResponseBody User signup(@Valid @RequestBody UserForm userForm) throws UserExistException {
		User user = userForm.extract();
		try{
			userService.addUser(user);
			
			System.out.println("----------------- 1");
			System.out.println(user.getId());
			System.out.println(user.getToken());
			System.out.println("----------------- 2");
			
		}catch(UserExistException e){
			System.out.println(e.getMessage());
			throw e;
		}
		return user;
	}

}
