package com.litekey.porun18.web.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.annotations.Filter;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

@Filter(name = "corsFilter")
public class CorsFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Max-Age", "10");
		String reqHead = request.getHeader("Access-Control-Request-Headers");
		if (!StringUtils.isEmpty(reqHead)) {
			response.addHeader("Access-Control-Allow-Headers", reqHead);
		}
		if (request.getMethod().equals("OPTIONS")) {
			response.getWriter().print("OK");
			response.getWriter().flush();
		} else {
			filterChain.doFilter(request, response);
		}
	}
}