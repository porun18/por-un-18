package com.litekey.porun18.web.forms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.validator.constraints.NotEmpty;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Ingredient;

public class FoodForm implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3699802424002632906L;

	@NotEmpty
	private String name;

	@NotEmpty
	private String ingredients;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public Food extract(){
		Food food = new Food();
		Set<Ingredient> ingredients = new HashSet<Ingredient>();
		
		
		String[] is = this.ingredients.split(",");
		
		for(int i=0; i<is.length; i++){
			Ingredient ingredient = new Ingredient();
			ingredient.setName(is[i]);
			ingredients.add(ingredient);
		}
		
		food.setIngredient(ingredients);
		food.setName(this.name);
		
		return food;
	}
	
}
