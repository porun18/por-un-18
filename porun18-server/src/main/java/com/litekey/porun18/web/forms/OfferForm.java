package com.litekey.porun18.web.forms;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Offer;

public class OfferForm implements Serializable {
	
	private static final long serialVersionUID = -4550369486176128957L;
	
	private Integer id;
	@NotEmpty
	private Integer foodId;
	@NotEmpty
	private Integer portions;
	@NotEmpty
	private Double price;
	private Boolean offer;
	@NotEmpty
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date start;
	@NotEmpty
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date end;

	public Offer extract(){
		Offer offerObj = new Offer();
		offerObj.setId(id);
		
		Food food = new Food();
		food.setId(foodId);
		
		offerObj.setFood(food);
		offerObj.setPortions(portions);
		offerObj.setAvailablePortions(portions);
		double p = price;
		offerObj.setPrice((float)p);
		if(offer == null){
			offerObj.setOffer(false);
		}else{
			offerObj.setOffer(offer);
		}
		Date date = new Date();
		offerObj.setDate(date);
		offerObj.setStart(start);
		offerObj.setEnd(end);
		return offerObj;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFood() {
		return foodId;
	}

	public void setFood(Integer foodId) {
		this.foodId = foodId;
	}

	public Integer getPortions() {
		return portions;
	}

	public void setPortions(Integer portions) {
		this.portions = portions;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getOffer() {
		return offer;
	}

	public void setOffer(Boolean offer) {
		this.offer = offer;
	}

	public Integer getFoodId() {
		return foodId;
	}

	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
}
