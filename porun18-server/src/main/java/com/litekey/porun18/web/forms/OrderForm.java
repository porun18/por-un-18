package com.litekey.porun18.web.forms;

import java.io.Serializable;

import com.litekey.porun18.model.Offer;
import com.litekey.porun18.model.Order;

public class OrderForm implements Serializable {
	
	private static final long serialVersionUID = -4550369486176128957L;
	
	private Integer id;
	private Integer offerId;
	private Integer portions;

	public Order extract(){
		Order order = new Order();
		order.setId(id);
		order.setPortions(portions);
		
		
		Offer offer = new Offer();
		offer.setId(offerId);
		order.setOffer(offer);
		return order;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public Integer getPortions() {
		return portions;
	}

	public void setPortions(Integer portions) {
		this.portions = portions;
	}

}
