package com.litekey.porun18.web.forms;

import java.io.Serializable;

import com.litekey.porun18.model.Food;
import com.litekey.porun18.model.Rating;
import com.litekey.porun18.model.User;

public class RatingForm implements Serializable {

	private static final long serialVersionUID = 3511747714293845216L;
	
	private Integer sellerId;

	private Integer foodId;
	
	private Integer rating;


	public Integer getSellerId() {
		return sellerId;
	}

	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	public Integer getFoodId() {
		return foodId;
	}

	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Rating extract(){
		Rating r = new Rating();
		
		if(foodId != null && foodId > 0){
			Food f = new Food();
			f.setId(foodId);
			r.setFood(f);
		}
		
		if(sellerId != null && sellerId > 0){
			User u = new User();
			u.setId(sellerId);
			r.setSeller(u);
		}
		
		r.setRating(rating);
		return r;
	}
	
}
