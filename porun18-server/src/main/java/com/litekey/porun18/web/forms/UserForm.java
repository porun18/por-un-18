package com.litekey.porun18.web.forms;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.litekey.porun18.model.User;

public class UserForm implements Serializable {

	private static final long serialVersionUID = 3511747714293845216L;

	@NotEmpty
	private String email;

	@NotEmpty
	//@Email
	private String password;

	public String getemail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User extract(){
		User user = new User();
		user.setEmail(email);
		user.setPassword(password);
		return user;
	}
	
}
